package scloud

// MediaTranscoding is single transcoding for a track
type MediaTranscoding struct {
	Duration int `json:"duration"`

	Format struct {
		MimeType string `json:"mime_type"`
		Protocol string `json:"protocol"`
	} `json:"format"`

	Preset  string `json:"preset"`
	Quality string `json:"quality"`
	Snipped bool   `json:"snipped"`

	URL string `json:"url"`
}

// Track from SoundCloud API
type Track struct {
	Embedded Embedded `json:"_embedded"`

	Title              string `json:"title"`
	Description        string `json:"description"`
	ArtworkURLTemplate string `json:"artwork_url_template"`

	Duration     int `json:"duration"`
	FullDuration int `json:"full_duration"`
	SnipDuration int `json:"snip_duration"`

	Media struct {
		Transcodings []MediaTranscoding `json:"transcodings"`
	} `json:"media"`
	StreamURL string `json:"stream_url"`

	WaveformURL  string   `json:"waveform_url"`
	PermalinkURL string   `json:"permalink_url"`
	URN          string   `json:"urn"`
	StationURNs  []string `json:"station_urns"`
	UserTags     []string `json:"user_tags"`

	CreatedAt         string `json:"created_at"`
	Snipped           bool   `json:"snipped"`
	Blocked           bool   `json:"blocked"`
	Monetizable       bool   `json:"monetizable"`
	Commentable       bool   `json:"commentable"`
	Policy            string `json:"policy"`
	SubHighTier       bool   `json:"sub_high_tier"`
	SubMidTier        bool   `json:"sub_mid_tier"`
	AdSupported       bool   `json:"ad_supported"`
	DisplayStats      bool   `json:"display_stats"`
	MonetizationModel string `json:"monetization_model"`
	Syncable          bool   `json:"syncable"`
}

// GetPermalink conforms to Permalinkable
func (t Track) GetPermalink() string {
	return t.PermalinkURL
}
