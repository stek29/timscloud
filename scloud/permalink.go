package scloud

import (
	"errors"
	"net/url"
)

const permalinkURLBase = "https://soundcloud.com"

// Permalinkable types can be permalinked:
//   - Track
//   - User
//   - Playlist
type Permalinkable interface {
	GetPermalink() string
}

// GetByPermalink gets Permalinkable resource from permalink URL
func (c *Client) GetByPermalink(permalink string) (Permalinkable, error) {
	var resp struct {
		Track    *Track    `json:"track"`
		Playlist *Playlist `json:"playlist"`
		User     *User     `json:"user"`
	}

	q := url.Values{}
	q.Set("url", permalink)

	err := c.Request("permalink", q, &resp)
	if err != nil {
		return nil, err
	}

	var result Permalinkable

	switch {
	case resp.Track != nil:
		result = resp.Track
	case resp.Playlist != nil:
		result = resp.Playlist
	case resp.User != nil:
		result = resp.User
	default:
		err = errors.New("resp is not track, playlist nor user")
	}

	return result, err
}
