package scloud

import "strings"

// ImageSize is size of artwork or avatar
type ImageSize = string

const (
	// ImageSize500 is 500×500px image
	ImageSize500 ImageSize = "t500x500"
	// ImageSizeCrop is 400×400px image
	ImageSizeCrop ImageSize = "crop"
	// ImageSize300 is 300×300px image
	ImageSize300 ImageSize = "t300x300"
	// ImageSizeLarge is 100×100px image and is default size
	ImageSizeLarge ImageSize = "large"
	// ImageSize67 is 67×67px image and is only available on artworks
	ImageSize67 ImageSize = "t67x67"
	// ImageSizeBadge is 47×47px image
	ImageSizeBadge ImageSize = "badge"
	// ImageSizeSmall is 32×32px image
	ImageSizeSmall ImageSize = "small"
	// ImageSizeTiny is 20×20px image on artworks and 18×18px on avatars
	ImageSizeTiny ImageSize = "tiny"
	// ImageSizeMini is 16×16px image
	ImageSizeMini ImageSize = "mini"
)

// GetImageURL generates artwork or avatar url from template for selected size
//
// pass empty string for default size (large)
func GetImageURL(templateURL string, size ImageSize) string {
	if size == "" {
		size = ImageSizeLarge
	}
	return strings.Replace(templateURL, "{size}", size, 1)
}
