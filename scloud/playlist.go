package scloud

import "encoding/json"

// Playlist from SoundClound API
type Playlist struct {
	Embedded Embedded `json:"_embedded"`

	Title       string `json:"title"`
	Duration    int    `json:"duration"`
	ReleaseDate string `json:"release_date"`
	Genre       string `json:"genre"`

	TrackCount int `json:"track_count"`
	// TODO(stek29): enum
	SetType          string `json:"set_type"`
	IsAlbum          bool   `json:"is_album"`
	IsExplicit       bool   `json:"is_explicit"`
	IsSystemPlaylist bool   `json:"is_system_playlist"`

	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`

	Sharing string `json:"sharing"`

	ArtworkURLTemplate string   `json:"artwork_url_template"`
	PermalinkURL       string   `json:"permalink_url"`
	URN                string   `json:"urn"`
	UserURN            string   `json:"user_urn"`
	UserTags           []string `json:"user_tags"`
}

// GetPermalink conforms to Permalinkable
func (p Playlist) GetPermalink() string {
	return p.PermalinkURL
}

type PlaylistInfo struct {
	Playlist Playlist `json:"playlist"`
	Tracks   []Track  `json:"collection"`
}

// GetPlaylistInfo returns playlist related info by its urn
func (c *Client) GetPlaylistInfo(playlistURN string) (*PlaylistInfo, error) {
	var resp struct {
		Playlist Playlist `json:"playlist"`
		Tracks   struct {
			Collection []Track         `json:"collection"`
			Links      json.RawMessage `json:"_links"` // unknown
		} `json:"tracks"`
	}

	err := c.Request("playlists/"+playlistURN+"/info", nil, &resp)
	if err != nil {
		return nil, err
	}

	return &PlaylistInfo{
		Playlist: resp.Playlist,
		Tracks:   resp.Tracks.Collection,
	}, nil
}
