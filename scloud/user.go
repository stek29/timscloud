package scloud

// User from SoundCloud API
type User struct {
	ID          int    `json:"id"`
	URI         string `json:"uri"`
	Description string `json:"description"`
	URN         string `json:"urn"`
	Permalink   string `json:"permalink"`
	CreatedAt   string `json:"created_at"`

	Username  string `json:"username"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Verified  bool   `json:"verified"`
	IsPro     bool   `json:"is_pro"`

	FollowersCount  int `json:"followers_count"`
	FollowingsCount int `json:"followings_count"`
	TracksCount     int `json:"tracks_count"`

	StationURNs []string `json:"station_urns"`

	AvatarURL         string `json:"avatar_url"`
	AvatarURLTemplate string `json:"avatar_url_template"`
	VisualURLTemplate string `json:"visual_url_template"`
}

// GetPermalink conforms to Permalinkable
func (u User) GetPermalink() string {
	return permalinkURLBase + "/" + u.Permalink
}
