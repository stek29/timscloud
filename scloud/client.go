package scloud

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

const (
	defaultBaseURL   = "https://api-mobile.soundcloud.com/"
	defaultUserAgent = "SoundCloud/5.71.0"
)

// Client is used for interaction with SoundCloud API
type Client struct {
	ClientID string

	client *http.Client
}

// NewClient creates new client with SoundCloud API client_id and
// http.Client used for API requests (pass nil for default client)
func NewClient(clientID string, client *http.Client) *Client {
	if client == nil {
		client = http.DefaultClient
	}

	return &Client{
		ClientID: clientID,
		client:   client,
	}
}

// RawRequest makes GET request to rURL with stored http.Client and
// sets client_id in addition to query and sets User-Agent
//
// querystring in rURL is cleared
func (c *Client) RawRequest(rURL string, query url.Values) (io.ReadCloser, error) {
	u, err := url.Parse(rURL)
	if err != nil {
		return nil, err
	}

	if query == nil {
		query = url.Values{}
	}

	query.Set("client_id", c.ClientID)
	u.RawQuery = query.Encode()

	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("User-Agent", defaultUserAgent)

	r, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	if r.StatusCode != http.StatusOK {
		err = fmt.Errorf("StatusCode not %d but %d", http.StatusOK, r.StatusCode)
	}

	if err != nil {
		r.Body.Close()
		return nil, err
	}

	return r.Body, nil
}

// Request makes RawRequest with defaultBaseURL and decodes json to resp
func (c *Client) Request(method string, query url.Values, resp interface{}) error {
	body, err := c.RawRequest(defaultBaseURL+method, query)

	if err != nil {
		return err
	}
	defer body.Close()

	dec := json.NewDecoder(body)
	if err := dec.Decode(resp); err != nil {
		return err
	}

	return nil
}
