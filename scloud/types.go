package scloud

// Stats hold interaction counts for playlist or track
type Stats struct {
	CommentsCount int `json:"comments_count"`
	LikesCount    int `json:"likes_count"`
	PlaybackCount int `json:"playback_count"`
	RepostsCount  int `json:"reposts_count"`
}

// Embedded holds embedded data with user and stat for Track or Playlist
type Embedded struct {
	Stats Stats `json:"stats"`
	User  User  `json:"user"`
}
