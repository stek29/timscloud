package main

import (
	"net/http"

	"gitlab.com/stek29/timscloud/scloud"
	tele "gopkg.in/telebot.v3"
)

type soundBot struct {
	*tele.Bot
	*scloud.Client

	hc *http.Client
}

// Init sets all handlers and prepares bot to be started
func (b *soundBot) Init() error {
	b.Handle("/help", b.onHelp)
	b.Handle("/start", b.onHelp)
	b.Handle(tele.OnText, b.onMessage)

	return nil
}
