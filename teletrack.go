package main

import (
	"errors"
	"gitlab.com/stek29/timscloud/scloud"
	"strings"
)

// TeleTrack holds data which is enough to send tsrack on Telegram
type TeleTrack struct {
	StreamURL string
	ThumbURL  string
	Title     string
	Performer string
	Duration  int
}

// FromScloud fills TeleTrack from SoundCloud Track
//
// StreamURL should be fetched with same scloud Client as Track
func (t *TeleTrack) FromScloud(st *scloud.Track) error {
	t.Title = st.Title

	t.Performer = scUserToPerformer(&st.Embedded.User)
	t.ThumbURL = scloud.GetImageURL(st.ArtworkURLTemplate, scloud.ImageSize300)

	t.Duration = 0
	for _, tr := range st.Media.Transcodings {
		if tr.Format.Protocol == "hls" {
			continue
		}

		if tr.Format.MimeType != "audio/mpeg" {
			continue
		}

		t.StreamURL = tr.URL
		// Convert milliseconds to seconds
		t.Duration = tr.Duration / 1000
	}

	if t.Duration == 0 {
		trs := make([]string, len(st.Media.Transcodings))
		for i, tr := range st.Media.Transcodings {
			trs[i] = tr.Format.MimeType + "," + tr.Format.Protocol
		}

		return errors.New("Cant find any non-hls mpeg transcoding: " + strings.Join(trs, ";"))
	}

	return nil
}

func scUserToPerformer(u *scloud.User) string {
	p := strings.TrimSpace(u.FirstName + " " + u.LastName)
	if p == "" {
		// Fallback to username if first/last name are unset
		p = u.Username
	}
	return p
}
