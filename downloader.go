package main

import (
	"errors"
	"fmt"
	"regexp"

	"gitlab.com/stek29/timscloud/scloud"
	tele "gopkg.in/telebot.v3"
)

var soundCloudRegexp = regexp.MustCompile(`\b(?:https?://)?soundcloud.com/(.+)\b`)
var shortLinkRegexp = regexp.MustCompile(`\b(?:soundcloud\.app\.goo\.gl|on\.soundcloud\.com)/\w+\b`)

func (b *soundBot) sendSCTrack(ctx tele.Context, st *scloud.Track) error {
	var track TeleTrack
	err := track.FromScloud(st)
	if err != nil {
		return fmt.Errorf("failed to convert track: %w", err)
	}

	audio := tele.Audio{
		Title:     track.Title,
		Performer: track.Performer,
		Duration:  track.Duration,

		Caption: fmt.Sprintf("@%s\n%s", b.Me.Username, st.PermalinkURL),
	}

	trackReader, err := b.Client.RawRequest(track.StreamURL, nil)
	if err != nil {
		return fmt.Errorf("failed to download track: %w", err)
	}
	defer trackReader.Close()
	audio.File.FileReader = trackReader

	thumbReader, err := b.Client.RawRequest(track.ThumbURL, nil)
	if err != nil {
		return fmt.Errorf("failed to download artwork: %w", err)
	}
	defer thumbReader.Close()
	audio.Thumbnail = &tele.Photo{File: tele.FromReader(thumbReader)}

	r, err := b.Reply(ctx.Message(), &audio)

	if err != nil {
		return fmt.Errorf("failed to send track: %w", err)
	}

	if audio.File.FileReader != nil {
		// if file was sent from reader, save file_id
		if r.Audio == nil {
			return errors.New("failed to extract sent audio")
		}
	}

	return nil
}

func (b *soundBot) sendSCPlaylistInfo(ctx tele.Context, pl *scloud.Playlist) error {
	ph := tele.Photo{
		File: tele.FromURL(scloud.GetImageURL(pl.ArtworkURLTemplate, scloud.ImageSize500)),
		Caption: fmt.Sprintf(
			"%s\n%d tracks\n@%s\n%s",
			scUserToPerformer(&pl.Embedded.User),
			pl.TrackCount,
			b.Me.Username,
			pl.GetPermalink()),
	}
	err := ctx.Reply(&ph)
	if err != nil {
		return fmt.Errorf("failed to send playlist info: %w", err)
	}
	return nil
}

func (b *soundBot) resolveShortLink(url string) (string, error) {
	resp, err := b.hc.Head(url)
	if err != nil {
		return "", err
	}
	finalURL := resp.Request.URL.String()
	resp.Body.Close()
	return finalURL, nil
}

func (b *soundBot) onMessage(ctx tele.Context) error {
	linkText := ctx.Text()

	if sMatch := shortLinkRegexp.FindStringSubmatch(linkText); len(sMatch) != 0 {
		shortURL := "https://" + sMatch[0]
		if rLink, err := b.resolveShortLink(shortURL); err == nil {
			linkText = rLink
		}
	}

	match := soundCloudRegexp.FindStringSubmatch(linkText)
	if len(match) < 1 {
		if ctx.Chat().Type == tele.ChatPrivate {
			b.onHelp(ctx)
		}
		return nil
	}

	permalink := "https://soundcloud.com/" + match[1]

	pl, stracks, err := b.fetchSCPermalink(permalink)
	if err != nil {
		_ = ctx.Reply(fmt.Sprintf("failed to get permalink `%s`: %v", permalink, err))
		return nil
	}

	if pl != nil {
		err = b.sendSCPlaylistInfo(ctx, pl)
		if err != nil {
			_ = ctx.Reply(err.Error())
		}
	}

	for i := range stracks {
		err = b.sendSCTrack(ctx, &stracks[i])
		if err != nil {
			_ = ctx.Reply(ctx, err.Error())
		}
	}

	return nil
}

func (b *soundBot) fetchSCPermalink(permalink string) (*scloud.Playlist, []scloud.Track, error) {
	p, err := b.Client.GetByPermalink(permalink)
	if err != nil {
		return nil, nil, err
	}

	switch v := p.(type) {
	case *scloud.Track:
		return nil, []scloud.Track{*v}, nil
	case *scloud.Playlist:
		info, err := b.Client.GetPlaylistInfo(v.URN)
		if err != nil {
			return nil, nil, err
		}

		return &info.Playlist, info.Tracks, nil
	default:
		return nil, nil, fmt.Errorf("expected Track, but got %T for link", v)
	}
}
