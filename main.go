package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/stek29/timscloud/scloud"
	tele "gopkg.in/telebot.v3"
)

const (
	teleTokenEnv      = "TELEGRAM_BOT_TOKEN"
	scloudClientIDEnv = "SOUNDCLOUD_CLIENT_ID"

	webhookListenEnv    = "WEBHOOK_LISTEN"
	webhookPublicURLEnv = "WEBHOOK_PUBLIC_URL"

	defaultWebhookListen   = ":8000"
	defaultLongPollTimeout = 10 * time.Second
)

func getPoller() tele.Poller {
	whPublicURL := os.Getenv(webhookPublicURLEnv)
	if whPublicURL == "" {
		log.Printf("%s unset -- using LongPoller", webhookPublicURLEnv)
		return &tele.LongPoller{Timeout: defaultLongPollTimeout}
	}

	whListen := os.Getenv(webhookListenEnv)
	if whListen == "" {
		whListen = defaultWebhookListen
		log.Printf("%s unset, listening on default", webhookListenEnv)
	}

	log.Printf("Using Webhook Poller on %s", whPublicURL)
	return &tele.Webhook{
		Listen: whListen,
		Endpoint: &tele.WebhookEndpoint{
			PublicURL: whPublicURL,
		},
	}
}

func main() {
	teleToken := os.Getenv(teleTokenEnv)
	scloudClientID := os.Getenv(scloudClientIDEnv)

	if teleToken == "" || scloudClientID == "" {
		log.Fatalf("%s and %s should be set", teleTokenEnv, scloudClientIDEnv)
	}

	b, err := tele.NewBot(tele.Settings{
		Token:  teleToken,
		Poller: getPoller(),
	})

	if _, ok := b.Poller.(*tele.LongPoller); ok {
		log.Printf("long poller used, removing webhook")
		b.RemoveWebhook()
	}

	if err != nil {
		log.Fatal("Failed to create TG Bot: ", err)
	}

	httpClient := &http.Client{
		Timeout: 5 * time.Second,
	}

	sc := scloud.NewClient(scloudClientID, httpClient)

	bot := soundBot{
		hc:     httpClient,
		Bot:    b,
		Client: sc,
	}

	err = bot.Init()
	if err != nil {
		log.Fatal("Failed to init soundBot: ", err)
	}

	bot.Start()
}
