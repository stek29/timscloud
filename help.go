package main

import (
	tele "gopkg.in/telebot.v3"
)

const (
	helpMessage string = `hi, i'm soundbot. send a link to soundcloud track, playlist or album to me and i'll send the track back to you.`
)

func (b *soundBot) onHelp(ctx tele.Context) error {
	return ctx.Reply(helpMessage)
}
